# _SNF-Streaming-ETL_
A streaming ETL for ingesting raw xml files into Redhisft cluster from Smart & Final

## Prerequisites
- [Java](https://java.com/en/download/)
- [Gradle](https://gradle.org/)
- [Scala](https://www.scala-lang.org/)

## Build and Demo process

### Build
`./gradlew clean build`
### Run
`./gradlew run`
### All Together
`./gradlew clean run`

## Create Shadow Jar
`./gradlew clean shadowJar`

## Run Spark Job
`spark-submit --class ai.hypersonix.etl.Main  --packages org.apache.spark:spark-avro_2.11:2.4.0 --master local[*] build/libs/snf-streaming-etl-1.0-SNAPSHOT-shadow.jar`

`spark-submit --class ai.hypersonix.etl.Main --deploy-mode cluster --master yarn  --packages org.apache.spark:spark-avro_2.11:2.4.0 build/libs/snf-streaming-etl-1.0-SNAPSHOT-shadow.jar "/users/omesh/hx/data/SnF/Checkpoint" "file:///Users/omesh/hx/data/SnF/xml_test"`