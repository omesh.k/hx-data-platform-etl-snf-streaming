package ai.hypersonix.etl

import org.apache.spark.streaming.StreamingContext

object Main extends InitSpark {
  def main(args: Array[String]): Unit = {
    if(args.length != 2) {
      System.err.println("Usage: Main <checkpoint_dir> <data_dir>")
      System.exit(1)
    }
    val Seq(cpDir, data_dir) = args.toSeq


    val snfXMLStream = new snfXMLStream(sc, sqlContext, cpDir, data_dir)
    val ssc = StreamingContext.getActiveOrCreate(cpDir, snfXMLStream.creatingFunc _)

    ssc.start()
    ssc.awaitTermination()
    ssc.stop(stopSparkContext = true, stopGracefully = true)
  }
}
