package ai.hypersonix.etl

import org.apache.spark._
import org.apache.spark.streaming._
import org.apache.spark.sql._

import org.apache.hadoop.io.{LongWritable, Text}
import com.databricks.spark.xml.{XmlInputFormat, XmlReader}
import org.apache.hadoop.fs.Path


class snfXMLStream(sc: SparkContext, sqlContext: SQLContext, cpDir: String, srcPath: String) {

  // import the functions defined in the object.
  import TextToStringStream._
  import LoadDFFromRedshift._
  import WriteDFToRedshift._

  def creatingFunc(): StreamingContext = {

    val rowTag = "<Transaction>"
    val endRowTag = "</Transaction>"
    val batchInterval = Seconds(5)
    val ssc = new StreamingContext(sc, batchInterval)

    // Set the active SQLContext so that we can access it statically within the foreachRDD
    SQLContext.setActive(sqlContext)
    ssc.checkpoint(cpDir)

    val conf = sc.hadoopConfiguration
    conf.set(XmlInputFormat.START_TAG_KEY, rowTag)
    conf.set(XmlInputFormat.END_TAG_KEY, endRowTag)
    conf.set("fs.s3a.access.key", "<aws_access_key>")
    conf.set("fs.s3a.secret.key", "<aws_secret_key>")


    val jdbcURL = "jdbc:redshift://hypersonix-private.ch5ybjyz4imw.us-west-2.redshift.amazonaws.com:5439/hxmaster?user=hxmaster&password=<password>>"
    val tempS3Dir = "s3a://hypersonixdata/etl/snf/_tempdir"
    val AWSIAMRole = "arn:aws:iam::391417672900:role/hsx-redshift-private"
    val olapTableName = "smartnfinal.olap_item_details_streaming_test"


    //    ------------------------ Streaming -------------------------
//    val stream = ssc.fileStream[LongWritable, Text, XmlInputFormat]("file:///Users/omesh/hx/data/SnF/xml_test/", (f: Path) => true, newFilesOnly = false, conf)
    val stream = ssc.fileStream[LongWritable, Text, XmlInputFormat](srcPath, (f: Path) => true, newFilesOnly = false, conf)


    //      .window(Minutes(1))
    stream.map(textToString).foreachRDD { rdd =>
      println("Processed messages in this batch: " + rdd.count())

      if(!rdd.isEmpty) {
        val _sqlContext = SQLContext.getOrCreate(rdd.sparkContext)
        val df = new XmlReader().withRowTag(rowTag).xmlRdd(_sqlContext, rdd)

        val itemDF = loadDFFromRedshift(_sqlContext, jdbcURL, tempS3Dir, "SELECT itemnumber, company_number, itemcreate_date, item_description FROM smartnfinal.dim_item")
        val storeDF = loadDFFromRedshift(_sqlContext, jdbcURL, tempS3Dir, "SELECT company_id, company_name, region_code, region, store_number, store_name FROM smartnfinal.dim_store")

        itemDF.createOrReplaceTempView("dim_item_from_redshift")
        storeDF.createOrReplaceTempView("dim_store_from_redshift")

        df.createOrReplaceTempView("raw_xml")
//        _sqlContext.sql("select * from raw_xml").show()

        val sql_transaction_check = "WITH cte_retail_transaction_total\nAS\n(\n  SELECT  `SES:InternalTransactionID`\n          ,MAX(CASE WHEN RetailTransactionTotal._TotalType = 'TransactionGrandAmount' THEN RetailTransactionTotal._VALUE END) AS TransactionGrandAmount\n          ,MAX(CASE WHEN RetailTransactionTotal._TotalType = 'TransactionNetAmount' THEN RetailTransactionTotal._VALUE END) AS TransactionNetAmount\n          ,MAX(CASE WHEN RetailTransactionTotal._TotalType = 'TransactionTaxAmount' THEN RetailTransactionTotal._VALUE END) AS TransactionTaxAmount\n  FROM    raw_xml a\n          LATERAL VIEW explode(RetailTransaction.Total) b AS RetailTransactionTotal\n  GROUP BY `SES:InternalTransactionID`\n)\nSELECT  RetailStoreID AS StoreNumber\n        ,WorkstationID AS LaneNumber\n        ,TillID AS POSCardNumber\n        ,SequenceNumber AS TransactionNumber\n        ,BusinessDayDate AS BusinessDate\n        ,BeginDateTime AS TransactionDate\n        ,BeginDateTime AS TransactionTime\n        ,OperatorID._WorkerID AS OperatorID\n        ,b.TransactionGrandAmount AS TotalTransactionAmount\n        ,b.TransactionNetAmount AS TotalCostAmount\n        ,b.TransactionTaxAmount AS TotalSalesTaxAmount\n        ,_TrainingModeFlag AS BusinessFlag\n        ,\"Extract from File Name\" AS Channel\nFROM    raw_xml a\n        LEFT JOIN cte_retail_transaction_total b\n        ON a.`SES:InternalTransactionID` = b.`SES:InternalTransactionID`\nWHERE   RetailTransaction._TransactionStatus = 'Finished'\n        AND _TrainingModeFlag = 'false'\n        AND _CancelFlag = 'false'"
        _sqlContext.sql(sql_transaction_check).createOrReplaceTempView("transaction_check")
//        _sqlContext.sql("select * from transaction_check").show()

        val sql_transaction_item_details = "SELECT  RetailStoreID AS StoreNumber\n        ,WorkstationID AS LaneNumber\n        ,TillID AS POSCardNumber\n        ,SequenceNumber AS TransactionNumber\n        ,BusinessDayDate AS BusinessDate\n        ,BeginDateTime AS TransactionDate\n        ,BeginDateTime AS TransactionTime\n        ,OperatorID._WorkerID AS OperatorID\n        ,lineitems.Sale.ItemID._VALUE AS ItemNumber\n        ,lineitems.Sale.Quantity._VALUE AS ItemQuantity\n        ,lineitems.Sale.ExtendedAmount AS ItemTotalSalesPriceAmount\n        ,lineitems.Sale.RegularSalesUnitPrice AS ItemTotalCostAmount\nFROM    raw_xml a\n        LATERAL VIEW explode(RetailTransaction.LineItem) b AS lineitems"
        _sqlContext.sql(sql_transaction_item_details).createOrReplaceTempView("transaction_item_details")
//        _sqlContext.sql("select * from transaction_item_details").show()

        val enrichedTransactionItemDF = _sqlContext.sql("" +
          "SELECT tid.StoreNumber, tid.LaneNumber, tid.POSCardNumber, tid.TransactionNumber, tid.BusinessDate, tid.TransactionDate, tid.TransactionTime, tid.OperatorID, tid.ItemNumber, tid.ItemQuantity, tid.ItemTotalSalesPriceAmount, tid.ItemTotalCostAmount, NULL AS TotalTransactionAmount, NULL AS TotalCostAmount, NULL AS TotalSalesTaxAmount, NULL AS BusinessFlag, NULL AS Channel " +
          "     , di.company_number, di.itemcreate_date, di.item_description " +
          "     , ds.company_id, ds.company_name, ds.region_code, ds.region, ds.store_name " +
          "     , 1 AS record_type " +
          "FROM transaction_item_details tid " +
          "LEFT JOIN dim_item_from_redshift di ON tid.ItemNumber = di.itemnumber " +
          "LEFT JOIN dim_store_from_redshift ds ON tid.StoreNumber = ds.store_number " +
          "UNION ALL " +
          "SELECT tc.StoreNumber, tc.LaneNumber, tc.POSCardNumber, tc.TransactionNumber, tc.BusinessDate, tc.TransactionDate, tc.TransactionTime, tc.OperatorID, NULL AS ItemNumber, NULL AS ItemQuantity, NULL AS ItemTotalSalesPriceAmount, NULL AS ItemTotalCostAmount, tc.TotalTransactionAmount, tc.TotalCostAmount, tc.TotalSalesTaxAmount, tc.BusinessFlag, tc.Channel " +
          "     , NULL AS company_number, NULL AS itemcreate_date, NULL AS item_description " +
          "     , ds.company_id, ds.company_name, ds.region_code, ds.region, ds.store_name " +
          "     , 2 AS record_type " +
          "FROM transaction_check tc " +
          "LEFT JOIN dim_store_from_redshift ds ON tc.StoreNumber = ds.store_number " +
          "").cache()

        enrichedTransactionItemDF.show()

        writeDFtoRedshift(enrichedTransactionItemDF, jdbcURL, tempS3Dir, AWSIAMRole, olapTableName)
      }
    }

    ssc
  }
}

object TextToStringStream extends Serializable {
  def textToString(value: (LongWritable, Text)): String = {
    value._2.toString
  }
}

object LoadDFFromRedshift extends Serializable {
  def loadDFFromRedshift(sqlContext: SQLContext, jdbcURL: String, tempS3Dir: String, sql: String): DataFrame = {
    sqlContext.read
      .format("io.github.spark_redshift_community.spark.redshift")
      .option("url", jdbcURL) //Provide the JDBC URL
      .option("tempdir", tempS3Dir) //User provides a temporary S3 folder
      .option("forward_spark_s3_credentials", "true")
      .option("query", sql)
      .load()
      .checkpoint(eager = true)
  }
}

object WriteDFToRedshift extends Serializable {
  def writeDFtoRedshift(df: DataFrame, jdbcURL: String, tempS3Dir: String, awsIAMRole: String, tableName: String): Unit = {
    df.write
      .format("io.github.spark_redshift_community.spark.redshift")
      .option("url", jdbcURL)
      .option("dbtable", tableName)
      .option("aws_iam_role", awsIAMRole)
      .option("tempdir", tempS3Dir)
      .mode("append")
      .save()
  }
}
